;"Main"
(setf *read-default-float-format* 'double-float)
(setf *random-state* (make-random-state t))

(defstruct n
	;a vector input weights; n in the previous layer"
	int-weights
	(input-weight 1.0d0 :type double-float)
	(f-input 1.0d0 :type double-float)
	g ;"activation"
	dg ;"derivate of activation"
	(in 1.0d0 :type double-float) ;"weight sum of inputs"
	(a 1.0d0 :type double-float) ;"ouput a = g(in)"
	(delta 0.0d0 :type double-float) ;"delta for backpropogation"
)
(defun sigmoid (x)
	(/ 1 (1+ (exp (- x))))
)

(defun dsigmoid (x)
	(let ((y (sigmoid x)))
		(* (- 1 y) y)
	)
)

(defun make-network (sizes &key (g #'sigmoid) (dg #'dsigmoid) (f-input -1.0d0) (weights))
	;"create a neural network as a list of n; each list is a layer"
	(let ((network))
		(do* ((pre-size nil (car sizes))
			(sizes sizes (cdr sizes))
			(size (car sizes) (car sizes))
			(layer () ()))
			((null size))
			(dotimes (i size)
				(let ((int-weights (make-array pre-size :element-type 'double-float :initial-element 0.0d0))
						(input-weight (- (random 1.0d0) 0.5)))
						(if pre-size
							(progn
								(if weights (setf input-weight (pop weights)))
								(dotimes (i pre-size)
									(setf (elt int-weights i)
										(if weights (pop weights) (- (random 1.0d0) 0.5))))))
								(push (make-n :int-weights int-weights :g g :dg dg :in f-input
										:f-input f-input :input-weight input-weight) layer)))
								(push (reverse layer) network))
							(reverse network)
	)
)

(defun string-to-list (s)
  (let ((L (read-from-string 
           (concatenate 'string "(" s ")"))))
    L))


(defun load-network (filename)
	(let ((in (open filename))
		(num-inputs)
		(num-hidden)
		(num-outputs)
		(line)
		(weights))
	(setf line (string-to-list (read-line in)))
	(setf num-inputs (first line)
			num-hidden (second line)
			num-outputs (third line))
	(run (i (+ num-hidden num-outputs))
		(setf line (string-to-list (read-line in)))
		(setf weights (append weights line)))
	(make-network (list num-inputs num-hidden num-outputs) :weights weights))
)

(defun write-network (network filename)
	(with-open-file(out filename :direction :output :if-exists :supersede :if-does-not-exist :create)
	(let ((sizes (mapcar #'length network))
		(weights (mapcar #'(lambda (layer)
				(mapcar #'(lambda (p)
						(cons (n-input-weight p)
								(coerce '(n-int-weights) 'list)))
								layer))
							(cdr network)))) ;"skip the input layer"
				(format out "~{~d~^ ~}~%" sizes)
				(format out "~{~{~{~,3f~^ ~}~%~}~}" weights)))
)

(defun load-data (filename)
	(let ((in (open filename))
			(num-data)
			(num-inputs)
			(num-outputs)
			(line)
			(data)
			(inputs)
			(outputs))
		(setf line (string-to-list (read-line in)))
		(setf num-data (first line)
				num-inputs (second line)
				num-outputs (third line))
		(dotimes (i num-data)
			(setf line (string-to-list (read-line in)))
			(setf inputs (subseq line 0 num-inputs)
					outputs (subseq line num-inputs))
			(push (list inputs outputs) data))
			(reverse data))
)

(defun write-results (metrics filename)
	(with-open-file(out filename :direction :output :if-exists :supersede :if-does-not-exist :create)
	(setf metrics
			(mapcar #'(lambda (metric)
						(let* ((a (first metric))
								(b (second metric))
								(c (third metric))
								(d (fouth metric))
								(accuracy (/ (+ a d) (+ a b c d)))
								(precision (/ a (+ a b)))
								(recall (/ a (+ a c)))
								(f1 (/ (* 2 precision recall) (+ precision recall))))
							(list a b c d accuracy precision recall f1)))
						metrics))
		(format out "~{~{~d ~d ~d ~d ~,3f ~,3f ~,3f ~,3f~%~}~}" metrics)
		;"averaging"
		(let* ((a (reduce #'+ metrics :key #'first))
				(b (reduce #'+ metrics :key #'second))
				(c (reduce #'+ metrics :key #'third))
				(d (reduce #'+ metrics :key #'fourth))
				(accuracy (/ (+ a d) (+ a b c d)))
				(precision (/ a (+ a b)))
				(recall (/ a (+ a c)))
				(f1 (/ (* 2 precision recall) (+ precision recall))))
			(format out  "~,3f ~,3f ~,3f ~,3f~%" accuracy precision recall f1)))
    (let* ((num-classes (length metrics))
           (accuracy (/ (reduce #'+ metrics :key #'fifth) num-classes))
           (precision (/ (reduce #'+ metrics :key #'sixth) num-classes))
           (recall (/ (reduce #'+ metrics :key #'seventh) num-classes))
           (f1 (/ (* 2 precision recall) (+ precision recall))))
      (format out "~,3f ~,3f ~,3f ~,3f~%" accuracy precision recall f1))
)


(defun input-set (network dtum)
	;"set outputs of input layer neurons"
	(mapc #'(lambda (n x)
			(setf (n-a n) (coerce x 'double-float)))
		(car network) (car dtum))
)

(defun layer-output (layer pre-layer)
	;"compute output of layer"
	(dolist (n layer)
		(setf (n-in n)
			(* (n-f-input n)
				(n-int-weights n)))
		(map nil #' (lambda (input-n w)
					(incf (n-in n)
						(* w (n-a input-n))))
					pre-layer (n-int-weights n))
				(setf (n-a n)
						(funcall (n-g n) (n-in n))))
)

(defun forpropagation (network)
	;"propogate inputs of network forward to compute outputs"
	(labels ((forprop (net pre-net)
				(if net 
						(progn
								(layer-output (car net) (car pre-net))
								(forprop (cdr net) (cdr pre-net))))))
				(forprop (cdr network) network))
)

(defun backpropagation (network dtum algo)
	;"propagate inputs of network forward to compute outputs, then
	;propogate deltas backward and update weights"
	(labels 
		((backprop (net pre-net dtum algo &key (depth 0))
			(if (null net)
			;"when net is null, reach the bottom and pre-net. contains only the output layer"
				(mapc #'(lambda (n y)
						(setf (n-delta n)
							(* (funcall (n-dg n) (n-in n))
								(- y (n-a n)))))
						(car pre-net) (cadr dtum))
				(progn ;"propogate inputs forward"
					(layer-output (car net) (car pre-net))
					(backprop (cdr net) (cdr pre-net) dtum algo :depth (1+ depth))
					;"propagate deltas backward and update weights on the way back up"
					(let ((layer (car pre-net))
						(next-layer (car net)))
						;"compute deltas for layer if layer is not input layer"
						(if (/= depth 0)
							(mapc #'(lambda (n r)
										(setf (n-delta n)
											(* (funcall (n-dg n) (n-in n))
												(reduce #'+ next-layer :key #' (lambda (output-n)
												(* (elt (n-int-weights output-n) r)
												(n-delta output-n)))))))
									layer (iota (length layer))))
									;"update input weights of next layer"
									(dolist (output-n next-layer)
									(incf (n-input-weight output-n)
									(* algo (n-f-input output-n) (n-delta output-n)))
							(setf (n-int-weights output-n)
								(map '(vector double-float) #' (lambda (w n)
									(+ w (* algo (n-a n) (n-delta output-n))))
								(n-int-weights output-n) layer))))))))
							(backprop (cdr network) network dtum algo :depth 0))
)

(defun learn (network data &key (algo 0.1d0) (eos 100))
	;"Train network on data using backpropagation"
	(do ((eo 0 (1+ eo)))
		((= eo eos) network)
		(dolist (dtum data)
			(input-set network dtum)
			(backpropagation network dtum algo))
	)
)

(defun think (network data &key (boolean-output nil))
	;"apply network to data. Return a list of listd, where each inner list contains the outputsfor a 
	;dtum"
	(let ((results)
	(metrics))
	(dolist (dtum data)
	;"set input layer outputs"
	(input-set network dtum)
	;"propagate inputs forward"
	(forpropagation network)
	(push (mapcar #'n-a (car (last network))) results))
	(setf results (reverse results))
	(if boolean-output (let ((expect-results (mapcar #'cadr data)))
		(setf results (mapcar #'(lambda (res) (mapcar #'round res)) results))
	(setf metrics
		;"count each symbol for each class"
		(mapcar #'(lambda (c-results)
			(list (count 'a c-results)
					(count 'b c-results)
					(count 'c c-results)
					(count 'd c-results)))
			(apply #'mapcar #'list (mapcar #'(lambda (result expect-results)
				(mapcar #'(lambda (r e-r)
					(cond ((= 1 r e-r) 'a)
							((> r e-r) 'b)
							((< r e-r) 'c)
							((= 0 r e-r) 'd)))
							result expect-results)) results expect-results))))))
		(values results metrics))
)

;"Run"

(let ((net-fname)
      (data-fname)
      (output-fname)
      (net)
      (data)
      (algo)
      (eos)
      (train-p)
      (sizes))
  (setf train-p (y-or-n-p "Train a neural network?"))

  (if (and train-p (y-or-n-p"Generate a neural network?"))
      (progn
          (format *query-io* "Enter the neural network output file: ")
          (force-output *query-io*)
          (setf net-fname (read-line *query-io*))

          (format *query-io* "Enter a list of sizes for the network (ex: (3 2 3)): ")
          (force-output *query-io*)
          (setf sizes (read-from-string (read-line *query-io*)))

          (setf net (make-network sizes))
          (write-network net net-fname))
      (progn
        (format *query-io* "Enter the neural network input file: ")
        (force-output *query-io*)
        (setf net-fname (read-line *query-io*))
        (setf net (load-network net-fname))))

  (format *query-io* "Enter the data file: ")
  (force-output *query-io*)
  (setf data-fname (read-line *query-io*))
  (setf data (load-data data-fname))

  (format *query-io* "Enter the output file: ")
  (force-output *query-io*)
  (setf output-fname (read-line *query-io*))

  (if train-p
      (progn 
        (format *query-io* "Enter the learning rate to use: ")
        (force-output *query-io*)
        (setf algo (read-from-string (read-line *query-io*)))

        (format *query-io* "Enter the number of epochs to simulate: ")
        (force-output *query-io*)
        (setf eos (read-from-string (read-line *query-io*)))

        (learn net data :algo algo :eos eos)
        (write-network net output-fname))
      (multiple-value-bind (results metrics)
          (think net data :boolean-output t)
        (declare (ignore results))
        (write-results metrics output-fname))))







